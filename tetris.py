#
# BSD 3-Clause License
#
# Copyright (c) 2018, Tomek D. Loboda All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
#    disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
#    following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
#    products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
# USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import curses
import numpy as np
import random
import time

from threading import Event, Thread


try:  # [RL] this import may fail due to smbus2 module being absent on the training machine
    from led_matrix.led8x8 import Img, Matrix, MatrixMotion
except ImportError:
    pass


# ======================================================================================================================
class GravityThread(Thread):
    T = 1.00

    evt_is_paused = Event()
    evt_do_stop   = Event()

    skip_n = False
        # Used to skip n calls to the target function. This is used in two scenarios. First, right after a pause to
        # avoid immediate block fall. Second, right after a new block is generated to make sure it stays on the very
        # top a little longer. The latter is there to give the player a fighting chance as the display size is small.

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, fn, t=T):
        super().__init__()

        self.fn = fn
        self.t  = t

        self.evt_is_paused.set()  # start paused

    # ------------------------------------------------------------------------------------------------------------------
    def join(self, timeout=None):
        self.evt_do_stop.set()
        self.resume()

        super().join(timeout)

    # ------------------------------------------------------------------------------------------------------------------
    def run(self):
        while not self.evt_do_stop.wait(self.t):
            if self.evt_is_paused.is_set():
                while self.evt_is_paused.wait(0.01):
                    pass

            if self.skip_n > 0:
                self.skip_n -= 1
            else:
                self.fn()

    # ------------------------------------------------------------------------------------------------------------------
    def pause(self):
        if self.evt_is_paused.is_set(): return
        self.evt_is_paused.set()

    # ------------------------------------------------------------------------------------------------------------------
    def resume(self):
        if not self.evt_is_paused.is_set(): return
        self.skip_n = 1
        self.evt_is_paused.clear()

    # ------------------------------------------------------------------------------------------------------------------
    def set_skip_n(self, n):
        self.skip_n = n


# ======================================================================================================================
class Tetris(object):
    '''
    This is an implementation of the classic game of Tetris.  It is to be played on a 8x8 LED matrix display.  It uses
    the LED Matrix display driver and a curses-based interface.  It also provides hooks for reinforcement learning
    algorithms (indicated in the code by [RL]).
    '''

    ACTIONS = {  # [RL]
        0: 'LEFT',
        1: 'RIGHT',
        2: 'DOWN',
        3: 'UP'
    }

    UI_X     = 4
    UI_Y_TIT = 2

    BLOCKS = [  # first element is the weight (corresponds to the probability of the block being generated)
        ( 4, [[1,1], [0,1]]),  # L-right
        ( 4, [[1,1], [1,0]]),  # L-left
        ( 7, [[1],   [1]]),    # long
        ( 2, [[1,1], [1,1]]),  # square
        (10, [[1]])            # dot
    ]

    SCORE_BLOCK    = 1        # fuse a block with the board
    SCORE_ROW      = [9, 19]  # clear one or more rows (+1 comes already as the block fusion score)
    SCORE_CLEAR_M1 = 50       # clear the entire board minus one row
    SCORE_CLEAR    = 100      # clear the entire board

    ROW_CLR_LIT_SEQ = [
        [2,1,0],                                                      # 0 rows cleared (a block fused with the board)
        [9,9,9,8,8,7,7,6,5,4,3,2,1,0],                                # 1 row  cleared
        [15,15,15,14,14,13,13,12,12,11,11,10,10,9,8,7,6,5,4,3,2,1,0]  # 2 rows cleared
    ]
    ROW_CLR_T = 0.025  # LED matrix breathe frame time; [s]

    SKIP_N = 5
        # skip this many gravity cycles right after a new block has been generated; due to small display size the
        # player may need this small advantage

    led        = None  # LED matrix
    led_thread = None  # LED thread
    led_evt    = None  # LED stop event

    scr = None  # curses screen

    width  = 8
    height = 8

    board = None  # two-dimentional list of integers: 0-empty; will be addressed by [column][row]

    block   = None  # current block
    block_x = 4
    block_y = 0

    grav = None  # gravity thread

    is_started = False
    is_ended   = False  # has at least one game been played? (used for showing the score)
    is_paused  = False
    is_score   = False  # is the score being displayed?

    score      = 0
    score_last = 0  # score associated with the very last action (game-related action, not UI action)

    n_actions_tot = 0  # total number of actions taken

    is_human = True  # [RL]
        # Is the game going to be played by a human or a computer agent?
        #
        # The gravity thread will be off for a non-human player (but the block will still fall automatically).
        # This is so to avoid extra overhead.
        #
        # Implies: do_vis = True

    do_vis = True  # [RL]
        # Should the game be visualized on the LED matrix? (set False if training an agent)
        #
        # A human player will always have visualization on. An agent may choose not to which is desirable in the
        # training phase where large numbers of games should be played in as short a time as possible.

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, is_human=True, do_vis=True, seed=None):
        self.is_human = is_human
        self.do_vis   = is_human or do_vis

        random.seed(seed)

    # ------------------------------------------------------------------------------------------------------------------
    def app_end(self):
        if self.do_vis:
            self.led_thread_stop()
            self.led.clr().set()

        if self.is_human:
            self.grav.resume()
            self.grav_stop()

            # Restore the terminal settings:
            curses.nocbreak()
            curses.echo()
            curses.endwin()

    # ------------------------------------------------------------------------------------------------------------------
    def app_start(self):
        if self.do_vis:
            self.led_evt = Event()
            self.led     = MatrixMotion(evt_stop=self.led_evt)

        if self.is_human:
            self.led_thread_start(lambda: self.led.image(Img.FACE_U_T, 0, 2, t_pre=0.75, t_post=0.75, t_frame=0.005, t_wait=5))
            self.grav_start()
            self.app_ui_init()
            self.app_run()
        elif self.do_vis:
            self.led.clr().set().lit(0)
            time.sleep(5)

    # ------------------------------------------------------------------------------------------------------------------
    def app_run(self):
        ''' The main event loop. '''

        while True:
            c = self.scr.getch()

            # Block control:
            if   c == curses.KEY_LEFT:  self.block_move(-1,0, True)
            elif c == curses.KEY_RIGHT: self.block_move( 1,0, True)
            elif c == curses.KEY_DOWN:  self.block_move( 0,1, True)
            elif c == curses.KEY_UP:    self.block_rotate()

            # Game control:
            elif c == ord(' '):
                if self.is_score:
                    self.game_score_toggle()
                    continue

                if not self.is_started:
                    self.game_start()
                else:
                    self.game_pause_toggle()
            elif c == ord('s'):
                self.game_score_toggle()
            elif c == ord('q') or c == 27:
                self.app_end()
                break

            self.scr.refresh()

    # ------------------------------------------------------------------------------------------------------------------
    def app_ui_init(self):
        os.environ.setdefault('ESCDELAY', '25')  # speed up ESC keypress

        self.scr = curses.initscr()

        # Change the terminal settings:
        curses.curs_set(0)
        curses.noecho()
        curses.cbreak()

        # Define colors:
        #curses.start_color()
        #curses.init_pair(1, curses.COLOR_WHITE,  curses.COLOR_BLACK)
        #curses.init_pair(2, curses.COLOR_GREEN,  curses.COLOR_BLACK)
        #curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        #curses.init_pair(4, curses.COLOR_RED,    curses.COLOR_BLACK)

        # Create the UI:
        self.scr.keypad(True)  # enable cursor keys
        self.scr.box()
        self.scr.addstr(self.UI_Y_TIT,     self.UI_X, 'LEDRIS - A tetris game on a LED matrix display', curses.A_UNDERLINE)
        self.scr.addstr(self.UI_Y_TIT + 3, self.UI_X, 'SPACE                  Start, restart, pause, and unpause game')
        self.scr.addstr(self.UI_Y_TIT + 4, self.UI_X, 'S                      Show score')
        self.scr.addstr(self.UI_Y_TIT + 4, self.UI_X, 'ESC or Q               Quit game')
        self.scr.addstr(self.UI_Y_TIT + 5, self.UI_X, 'LEFT + RIGHT + DOWN    Play the game')
        self.scr.refresh()

    # ------------------------------------------------------------------------------------------------------------------
    def block_draw(self, val):
        ''' Draws only on the LED matrix. '''

        if not self.do_vis: return

        for y in range(len(self.block)):
            for x in range(len(self.block[0])):
                if self.block[y][x] != 0:
                    self.led.px(self.block_x + x, self.block_y + y, val)

        if val != 0:
            self.led.set()

    # ------------------------------------------------------------------------------------------------------------------
    def block_drop(self):
        if not self.is_started or self.is_paused or self.block is None: return

        # TODO: Implement

        pass

    # ------------------------------------------------------------------------------------------------------------------
    def block_fuse(self):
        ''' Fuse the block with the board. '''

        self.score_last += self.SCORE_BLOCK

        for y in range(len(self.block)):
            for x in range(len(self.block[0])):
                if self.block[y][x] != 0:
                    self.board[self.block_y + y][self.block_x + x] = self.block[y][x]

        if self.is_human:
            self.led.breathe(1, 1, lit_seq=self.ROW_CLR_LIT_SEQ[0], t_frame=self.ROW_CLR_T, t_wait=0)

        # If an agent is learning, grade the negative reward for making the fused clump high:
        if (not self.is_human) and (not self.do_vis):
            self.score_last = self.block_y - self.height + 1

        # Reset the block and consolidate the display:
        self.block = None
        self.board_clr_rows()
        self.board_push_led()

        self.block_new()
        self.board_push_led()

        # Clear the keyboard buffer:
        if self.is_human:
            self.scr.nodelay(True)
            while self.scr.getch() != -1:
                pass
            self.scr.nodelay(False)

    # ------------------------------------------------------------------------------------------------------------------
    def block_is_valid(self, block, x, y):
        if block is None: return False

        # Outside:
        if (y < 0) or (y + len(block)    - 1 >= self.height): return False
        if (x < 0) or (x + len(block[0]) - 1 >= self.width ): return False

        # Collides with the board:
        for y_ in range(len(block)):
            for x_ in range(len(block[0])):
                if (block[y_][x_] != 0) and (self.board[y + y_][x + x_] != 0): return False

        return True

    # ------------------------------------------------------------------------------------------------------------------
    def block_move(self, dx, dy, do_force_unpause=False):
        if not self.is_started or self.is_score or self.block is None: return

        if self.is_paused:
            if do_force_unpause:
                self.game_pause_toggle()
            else: return

        self.n_actions_tot += 1
        self.score_last = 0

        if not self.block_is_valid(self.block, self.block_x + dx, self.block_y + dy):
            if dy > 0:
                self.block_fuse()
            return

        self.block_draw(0)
        self.block_x += dx
        self.block_y += dy
        self.block_draw(1)

    # ------------------------------------------------------------------------------------------------------------------
    def block_new(self):
        self.block = list(self.BLOCKS[random.choices(range(len(self.BLOCKS)), [b[0] for b in self.BLOCKS])[0]][1])
        self.block_x = self.width // 2 - len(self.block[0]) // 2
        self.block_y = 0

        if self.is_human:
            self.grav.set_skip_n(self.SKIP_N)

        self.block_draw(1)

        if not self.block_is_valid(self.block, self.block_x, self.block_y):
            self.game_end()

    # ------------------------------------------------------------------------------------------------------------------
    def block_rotate(self):
        if not self.is_started or self.is_score or self.block is None: return

        if self.is_paused:
            self.game_pause_toggle()

        self.n_actions_tot += 1
        self.score_last = 0

        block = [[self.block[y][x] for y in range(len(self.block))] for x in range(len(self.block[0]) - 1, -1, -1)]

        if not self.block_is_valid(block, self.block_x, self.block_y):
            return

        self.block_draw(0)
        self.block = block
        self.block_draw(1)

    # ------------------------------------------------------------------------------------------------------------------
    def board_clr_rows(self):
        n_rem = 0
        for y in range(self.height):
            if not 0 in self.board[y]:
                del self.board[y]
                self.board = [[0] * self.width] + self.board
                n_rem += 1

        if n_rem > 0:
            if n_rem > 2:
                print("ERROR  n_rem:{} score_last:{} score:{} n_actions_tot:{}".format(n_rem, self.score_last, self.score, self.n_actions_tot))
                n_rem = 2

            self.score_last += self.SCORE_ROW[n_rem - 1]
            if sum(self.board[self.height - 1]) == 0:
                self.score_last += self.SCORE_CLEAR
            elif sum(self.board[self.height - 2]) == 0:
                self.score_last += self.SCORE_CLEAR_M1

            if self.is_human:
                self.grav.pause()
                self.led.breathe(1, 1, lit_seq=self.ROW_CLR_LIT_SEQ[n_rem], t_frame=self.ROW_CLR_T, t_wait=0)

            if self.do_vis:
                self.board_push_led()

            if self.is_human:
                self.grav.resume()

        self.score += self.score_last

    # ------------------------------------------------------------------------------------------------------------------
    def board_push_led(self):
        ''' Write the board to the LED matrix. '''

        if not self.do_vis: return

        for y in range(self.height):
            for x in range(self.width):
                self.led.px(x,y, self.board[y][x])

        if self.block is not None:
            for y in range(len(self.block)):
                for x in range(len(self.block[0])):
                    if self.block[y][x] != 0:
                        self.led.px1(self.block_x + x, self.block_y + y)

        self.led.set()

    # ------------------------------------------------------------------------------------------------------------------
    def game_do_action(self, a):
        ''' [RL] '''

        if   a == 0: self.block_move(-1,0)
        elif a == 1: self.block_move( 1,0)
        elif a == 2: self.block_move( 0,1)
        elif a == 3: self.block_rotate()

        if self.n_actions_tot % 5 == 0:  # exert the gravitational force on the block every so many actions
            self.block_move(0,1)

        if self.do_vis:  # make agent gameplay visible for us, unworthy mortalz
            time.sleep(0.001)

        return (self.game_get_state(), self.score_last, self.is_ended, None)

    # ------------------------------------------------------------------------------------------------------------------
    def game_end(self):
        if not self.is_started: return

        if self.is_human:
            self.grav.pause()

        self.block = None

        self.is_started = False
        self.is_ended   = True

    # ------------------------------------------------------------------------------------------------------------------
    def game_get_state(self):
        '''
        [RL]

        The current block is embedded into the board and the whole thing becomes the state. No separate block info is
        given.
        '''

        if self.board is None: return None

        state = [i[:] for i in self.board]

        if self.block is not None:
            for y in range(len(self.block)):
                for x in range(len(self.block[0])):
                    if self.block[y][x] != 0:
                        state[self.block_y + y][self.block_x + x] = 1

        return np.array(state)

    # ------------------------------------------------------------------------------------------------------------------
    def game_is_over(self): return self.is_ended

    # ------------------------------------------------------------------------------------------------------------------
    def game_pause_toggle(self):
        if (not self.is_human) or (not self.is_started): return

        if self.is_paused:
            self.led_thread_stop()
            self.led.lit(0)
            self.grav.resume()
        else:
            self.grav.pause()
            self.led_thread_start(lambda: self.led.breathe(0, 1, lit_max=5, t_frame=0.025, t_wait=0))

        self.is_paused = not self.is_paused

    # ------------------------------------------------------------------------------------------------------------------
    def game_score_toggle(self):
        if (not self.is_human) or (not self.is_started and not self.is_ended): return

        if self.is_score:
            self.board_push_led()
        else:
            if self.is_started and not self.is_paused:
                self.game_pause_toggle()

            self.led.img(Img.SUIT_HEARTS)

        self.is_score = not self.is_score

    # ------------------------------------------------------------------------------------------------------------------
    def game_start(self, do_vis=None):
        if self.is_started: return
        
        if do_vis is not None:
            self.do_vis = self.is_human or do_vis

        if self.do_vis:
            self.led_thread_stop()
            self.led.clr().set().lit(0)

        self.board = [[0] * self.width for _ in range(self.height)]
        self.block_new()
        self.score         = 0
        self.score_last    = 0
        self.n_actions_tot = 0

        if self.is_human:
            self.grav.resume()

        self.is_started = True
        self.is_ended   = False

        return self.game_get_state()

    # ------------------------------------------------------------------------------------------------------------------
    def grav_start(self):
        if (not self.is_human) or (self.grav is not None): return

        self.grav = GravityThread(self.grav_step)
        self.grav.start()

    # ------------------------------------------------------------------------------------------------------------------
    def grav_step(self):
        if self.block is None:
            self.block_new()
        else:
            self.block_move(0,1)

    # ------------------------------------------------------------------------------------------------------------------
    def grav_stop(self):
        if (not self.is_human) or (self.grav is None): return

        self.grav.join()
        self.grav = None

    # ------------------------------------------------------------------------------------------------------------------
    def led_thread_start(self, fn):
        if (not self.do_vis) or (self.led_thread is not None): return

        self.led_thread = Thread(target=fn, daemon=True)
        self.led_thread.start()

    # ------------------------------------------------------------------------------------------------------------------
    def led_thread_stop(self):
        if (not self.do_vis) or (self.led_thread is None): return

        self.led_evt.set()
        self.led_thread.join()
        self.led_thread = None
        self.led_evt.clear()


# ======================================================================================================================
if __name__ == '__main__':
    try:
        t = Tetris()
        t.app_start()
    finally:
        curses.endwin()  # a precausion against screwing up the terminal during development
