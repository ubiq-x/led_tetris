# LED Tetris
Tetris for an 8x8 LED matrix display

## Introduction
This is an implementation of the classic game of Tetris.  It is to be played on a 8x8 LED matrix display.  It uses the [LED Matrix](https://gitlab.com/ubiq-x/led_matrix) display driver and a curses-based interface.  It also provides hooks for reinforcement learning algorithms but that complicates the code only slightly.


## Dependencies
- [numpy](http://www.numpy.org/)
- [LED Matrix](https://gitlab.com/ubiq-x/led_matrix)

## Setup
### Hardware
Same as [LED Matrix](https://gitlab.com/ubiq-x/led_matrix).

### Software
First, initialize a new Python venv and install the requirements:
```sh
venv=led_tetris
python=python3.6

mkdir -p ~/prj/
cd ~/prj
$python -m venv $venv
cd $venv
source ./bin/activate

python -m pip install numpy, smbus2

mkdir -p src
cd src
git clone https://gitlab.com/ubiq-x/led_matrix
git clone https://gitlab.com/ubiq-x/led_tetris
```

Then, run the game like so:
```sh
cd led_tetris
python tetris.py
```

Installing numpy on an SBC like the Raspberry Pi 3 takes a while and numpy is used only to return an array representing the game's state which is needed for reinforcement learning.  If you'd rather skip that part, comment out the import statement and the offending lines in `tetris.py`.  The game will still run.


## Gameplay
The game is played with the cursor keys: `<left>`, `<right>`, and `<down>` move the block while `<up>` rotates it.  Additionally, the `<space>` and `<q>` keys respectively pause/unpause and quit the game.

![Gameplay snapshot](media/tetris.jpg)


## License
This project is licensed under the [BSD License](LICENSE.md).
